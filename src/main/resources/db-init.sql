CREATE DATABASE IF NOT EXISTS referral;
USE referral;

DROP TABLE IF EXISTS referral;
DROP TABLE IF EXISTS referral_code;

CREATE TABLE referral (
  referral_id     INT UNSIGNED           AUTO_INCREMENT PRIMARY KEY,
  referred_uuid   VARCHAR(60)            UNIQUE NOT NULL,
  referrer_uuid   VARCHAR(60)            NOT NULL,
  timestamp       TIMESTAMP              DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE referral_code (
  referral_code_id   INT UNSIGNED          AUTO_INCREMENT PRIMARY KEY,
  referrer_uuid     VARCHAR(60)            UNIQUE NOT NULL,
  code              VARCHAR(60)            UNIQUE NOT NULL,
  timestamp         TIMESTAMP              DEFAULT CURRENT_TIMESTAMP
);