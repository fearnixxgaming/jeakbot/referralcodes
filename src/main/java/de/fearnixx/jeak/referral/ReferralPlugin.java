package de.fearnixx.jeak.referral;

import de.fearnixx.jeak.event.bot.IBotStateEvent;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.JeakBotPlugin;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.jeak.service.command.ICommandService;
import de.fearnixx.jeak.service.teamspeak.IUserService;
import de.fearnixx.jeak.teamspeak.IServer;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceUnit;

@JeakBotPlugin(id = "referral")
public class ReferralPlugin {

    @Inject
    private ICommandService commandService;

    @Inject
    private IUserService userService;

    @Inject
    private IServer server;

    @Inject
    @PersistenceUnit(name = "referral")
    private EntityManager entityManager;

    @Listener
    public void onInitialize(IBotStateEvent.IInitializeEvent event) {
        commandService.registerCommand(
                "referral",
                new ReferralCommandReceiver(entityManager, server, userService)
        );
    }
}
