package de.fearnixx.jeak.referral.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "ReferralCode")
@Table(name = "referral_code")
public class ReferralCode implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "referral_code_id")
    private Integer referralCodeId;

    @Column(name = "referrer_uuid")
    private String referrerUuid;

    @Column(name = "code")
    private String code;

    public Integer getReferralId() {
        return referralCodeId;
    }

    public String getReferrerUuid() {
        return referrerUuid;
    }

    public void setReferrerUuid(String referrerUuid) {
        this.referrerUuid = referrerUuid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String referralCode) {
        this.code = referralCode;
    }
}
