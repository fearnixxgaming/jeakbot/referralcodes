package de.fearnixx.jeak.referral.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity(name = "Referral")
@Table(name = "referral")
public class Referral implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "referral_id")
    private Integer referralId;

    @Column(name = "referred_uuid")
    private String referredUuid;

    @Column(name = "referrer_uuid")
    private String referrerUuid;

    @Column(name = "timestamp")
    private LocalDateTime timestamp;

    public Integer getReferralId() {
        return referralId;
    }

    public String getReferredUuid() {
        return referredUuid;
    }

    public void setReferredUuid(String referredUuid) {
        this.referredUuid = referredUuid;
    }

    public String getReferrerUuid() {
        return referrerUuid;
    }

    public void setReferrerUuid(String referrerUuid) {
        this.referrerUuid = referrerUuid;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }
}
