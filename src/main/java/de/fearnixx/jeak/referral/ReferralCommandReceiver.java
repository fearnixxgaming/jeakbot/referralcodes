package de.fearnixx.jeak.referral;

import de.fearnixx.jeak.referral.entity.Referral;
import de.fearnixx.jeak.referral.entity.ReferralCode;
import de.fearnixx.jeak.service.command.CommandException;
import de.fearnixx.jeak.service.command.ICommandContext;
import de.fearnixx.jeak.service.command.ICommandReceiver;
import de.fearnixx.jeak.service.teamspeak.IUserService;
import de.fearnixx.jeak.teamspeak.IServer;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.teamspeak.data.IUser;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class ReferralCommandReceiver implements ICommandReceiver {

    private static final String COMMAND_USAGE = "Usage of the 'referral' command: " + System.lineSeparator() +
            "!referral: Shows this text and your referral code" + System.lineSeparator() +
            "!referral <referralCode>: States that you were referred by the user with the given referral code" + System.lineSeparator() +
            "!referral <referredUuid> <referralCode/referrerUuid>: Can be used to state that the given client was referred by " +
            "the given client or the client with the given referral code";

    private final EntityManager entityManager;
    private final IUserService userService;

    private final IServer server;

    public ReferralCommandReceiver(EntityManager entityManager, IServer server, IUserService userService) {
        this.entityManager = entityManager;
        this.userService = userService;
        this.server = server;
    }

    @Override
    public void receive(ICommandContext ctx) throws CommandException {
        final int argSize = ctx.getArguments().size();
        final String invokerUID = ctx.getRawEvent().getInvokerUID();

        final IClient client = userService.getClientByID(ctx.getRawEvent().getInvokerId())
                .orElseThrow(
                        () -> new CommandException("Client not found!")
                );

        if (!client.hasPermission("referral.command.code")) {
            throw new CommandException("You do not have the permission to use this command");
        }

        if (argSize == 0) {
            server.getConnection().sendRequest(client.sendMessage(COMMAND_USAGE));

            entityManager.getTransaction().begin();

            try {
                ReferralCode referralCode;

                final Optional<ReferralCode> optReferralCode = optionalReferralCodeByUuid(invokerUID);

                if (optReferralCode.isPresent()) {
                    referralCode = optReferralCode.get();
                } else {
                    ReferralCode newReferralCode = new ReferralCode();
                    newReferralCode.setReferrerUuid(invokerUID);

                    final String code = UUID.randomUUID().toString();
                    newReferralCode.setCode(code);

                    entityManager.persist(newReferralCode);
                    entityManager.flush();

                    referralCode = newReferralCode;
                }

                entityManager.getTransaction().commit();

                server.getConnection().sendRequest(
                        client.sendMessage("Your referral code: " + referralCode.getCode())
                );

            } catch (Exception e) {
                entityManager.getTransaction().rollback();
                throw e;
            }

        } else if (argSize == 1) {
            entityManager.getTransaction().begin();

            try {
                checkUserAlreadyReferred(invokerUID);

                ReferralCode referralCode = optReferrerByReferralCode(ctx.getArguments().get(0))
                        .orElseThrow(
                                () -> new CommandException("The given referral code does not exist!")
                        );

                createReferral(invokerUID, referralCode.getReferrerUuid());
                entityManager.getTransaction().commit();

                server.getConnection().sendRequest(client.sendMessage("Your referral was submitted!"));
            } catch (Exception e) {
                entityManager.getTransaction().rollback();
                throw e;
            }

        } else if (argSize == 2) {
            final String referredUuid = ctx.getArguments().get(0);

            if (userService.findUserByUniqueID(referredUuid).isEmpty()) {
                throw new CommandException("The given user-uid of the referred client is unknown!");
            }

            try {
                entityManager.getTransaction().begin();

                checkUserAlreadyReferred(referredUuid);

                final String referralCodeOrReferrerUuid = ctx.getArguments().get(1);
                String referrerUuid;

                Optional<ReferralCode> optReferralCode = optReferrerByReferralCode(referralCodeOrReferrerUuid);

                if (optReferralCode.isPresent()) {
                    referrerUuid = optReferralCode.get().getReferrerUuid();
                } else {
                    final List<IUser> userByUniqueID = userService.findUserByUniqueID(referralCodeOrReferrerUuid);

                    if (userByUniqueID.isEmpty()) {
                        throw new CommandException("The given referral code or client does not exist!");
                    }

                    referrerUuid = userByUniqueID.get(0).getClientUniqueID();
                }

                createReferral(referredUuid, referrerUuid);
                entityManager.getTransaction().commit();

                server.getConnection().sendRequest(client.sendMessage("The referral was submitted!"));
            } catch (Exception e) {
                entityManager.getTransaction().rollback();
                throw e;
            }

        } else {
            throw new CommandException(
                    "Invalid number of arguments!"
            );
        }
    }

    private void createReferral(String referredUuid, String referrerUuid) throws CommandException {

        if (referredUuid.equals(referrerUuid)) {
            throw new CommandException("You can't refer yourself!");
        }

        Referral referral = new Referral();
        referral.setReferredUuid(referredUuid);
        referral.setReferrerUuid(referrerUuid);
        referral.setTimestamp(LocalDateTime.now());

        entityManager.persist(referral);
        entityManager.flush();
    }

    private Optional<ReferralCode> optionalReferralCodeByUuid(String uuid) {
        final TypedQuery<ReferralCode> referrerQuery =
                entityManager.createQuery(
                        "SELECT r FROM ReferralCode r WHERE r.referrerUuid = :uuid",
                        ReferralCode.class
                );

        referrerQuery.setParameter("uuid", uuid);

        final List<ReferralCode> referrerResults = referrerQuery.getResultList();
        if (referrerResults.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(referrerResults.get(0));
    }

    private Optional<ReferralCode> optReferrerByReferralCode(String referralCode) {
        final TypedQuery<ReferralCode> referrerQuery =
                entityManager.createQuery(
                        "SELECT r FROM ReferralCode r WHERE r.code = :code",
                        ReferralCode.class
                );

        referrerQuery.setParameter("code", referralCode);

        final List<ReferralCode> referrerResults = referrerQuery.getResultList();
        if (referrerResults.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(referrerResults.get(0));
    }

    private void checkUserAlreadyReferred(String uuid) throws CommandException {
        final TypedQuery<Referral> alreadyReferredQuery =
                entityManager.createQuery(
                        "SELECT r FROM Referral r WHERE r.referredUuid = :uuid",
                        Referral.class
                );

        alreadyReferredQuery.setParameter("uuid", uuid);

        if (!alreadyReferredQuery.getResultList().isEmpty()) {
            throw new CommandException("You have already been referred!");
        }
    }
}
